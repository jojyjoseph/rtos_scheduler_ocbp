# README #

The source file is a solo file for the scheduler to be implemented on Real Time extension of Linux, LITMUS.
### What is this repository for? ###

* Changes to scheduler algorithm for OCBP Real time scheduler.

### How do I get set up? ###

* Download the LITMUS Source from https://wiki.litmus-rt.org/litmus/InstallationInstructions
* Replace the scheduler file with the modified sched_ocbp.c
* Create task and check

### Contribution guidelines ###

* Writing tests
* Code review
* Project Report

### Who do I talk to? ###

* George Joseph