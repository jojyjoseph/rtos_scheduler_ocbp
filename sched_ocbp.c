/*
 * litmus/sched_fpps.c
 *
 * Implementation of partitioned fixed-priority scheduling.
 * Based on PSN-EDF.
 */

#include <linux/percpu.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/spinlock.h>
#include <linux/module.h>

#include <litmus/litmus.h>
#include <litmus/wait.h>
#include <litmus/jobs.h>
#include <litmus/preempt.h>
#include <litmus/fp_common.h>
#include <litmus/sched_plugin.h>
#include <litmus/sched_trace.h>
#include <litmus/trace.h>
#include <litmus/budget.h>

/* to set up domain/cpu mappings */
#include <litmus/litmus_proc.h>
#include <linux/uaccess.h>
#define OCBP_PROTOCOL
#define JOB_COUNT_MAX 	3


#ifdef OCBP_PROTOCOL


typedef enum{
	lo_mode,
	hi_mode
}Mode;

static long long gcd_ocbp(long long a, long long b)
{
	if (b ==0)
	return a;
	return gcd_ocbp(b,a%b);
}

static long long busy_interval_calc(long long a[], int n)
{
	long long res =1;
	int i;
	for(i=0;i<n;i++)
	{
		res = res *a[i]/gcd_ocbp(res,a[i]);
	}
	return res;
}

	
int task_count,job_count_sum=0;
int temp=0;
long long busy_interval,temp1=0,temp2=0,temp3=0,job_count[10] = {0,0,0,0,0,0,0,0,0,0}, temp4;
static long long start_clock, stop_clock;
static struct task_struct *ocbp_task_address[JOB_COUNT_MAX];
struct my_taskparam{
	double deadline;
	double c_low;
	double c_hi;
	unsigned int criticality;
	unsigned int prio;
	double period;
}task_ocbp[JOB_COUNT_MAX];
struct my_taskparam *taskarray[JOB_COUNT_MAX];


void jobs_count_in_busy_interval(struct my_taskparam *taskarray[], int num_tasks)
{
int i;

	for(i=0;i<num_tasks;i++)
	{
		job_count[i]= (busy_interval/taskarray[i]->period);
		TRACE("ocbp --gmc -- indv job count of job %d is %lld",i,job_count[i]);
		job_count_sum+=(job_count[i]);
	}
}

void busy_int(struct my_taskparam *taskarray[], int num_tasks)
{
int i;
int n=num_tasks;
struct my_taskparam *t[n];
long long period[n];

for(i=0;i<n;i++)
{
t[i] = taskarray[i];
t[i]->prio = 0;
}


// Busy interval calculation starts
	for(i=0;i<n;i++)
	{
		period[i] = t[i]->period;
	}
	busy_interval = busy_interval_calc(period,n);
	TRACE("ocbp -- gmc -- busy interval %lld \n",(busy_interval));
// Busy interval calculation ends

}
void priority_ocbp(struct my_taskparam *taskarray[], int num_tasks)
{

int i,j;
int n=num_tasks;
struct my_taskparam *t[n];
int sumlo,sumhi;

for(i=0;i<n;i++)
{
t[i] = taskarray[i];
t[i]->prio = 0;
}

// Audsley priority assignment
	for(i=0;i<n;i++)
	{
		sumlo=0;
		sumhi=0;

		for(j=0;j<n;j++)
		{
			if(t[j]->prio == 0)
			{
				sumlo = sumlo + t[j]->c_low;
				sumhi = sumhi + t[j]->c_hi;
			}
		}

		//Assignment priority

		for(j=0;j<n;j++)
		{
			if(t[j]->prio == 0)
			{
				if(t[j]->criticality == 0)
				{
					if( (t[j]->deadline - (sumlo - t[j]->c_low)) >= (t[j]->c_low) )
					t[j]->prio=n-i;
				}
				if(t[j]->criticality == 1)
				{
					if((t[j]->deadline -(sumhi - t[j]->c_hi) >= t[j]->c_hi))
						t[j]->prio = n-i;
				}
			}
		}
	}

	// Assign priorities to the task back
	{
		for(i=0;i<n;i++)
		{
			ocbp_task_address[i]->rt_param.task_params.priority= t[i]->prio;
			TRACE("ocbp -- gmc -- 2565 priority of job %d is  %d added \n",ocbp_task_address[i]->rt_param.task_params.pid,ocbp_task_address[i]->rt_param.task_params.priority);

		}
	}
}


#define task_critical(t)	(tsk_rt(t)->task_params.crit == HIGH)
#define wcet_hi(t)		(tsk_rt(t)->task_params.exec_cost_hi)
#define wcet_lo(t)		(tsk_rt(t)->task_params.exec_cost)
#define get_actual_exec_time(t)	(tsk_rt(t)->task_params.actual_exec_time)

inline static int nstoms(lt_t val)
{
	return (val/1000000);
}

#endif

typedef struct {
	rt_domain_t 		domain;
	struct fp_prio_queue	ready_queue;
	int          		cpu;
	struct task_struct* 	scheduled; /* only RT tasks */
#ifdef	OCBP_PROTOCOL
 
	Mode 			mode;


#define task_critical_check_high(t)	(tsk_rt(t)->task_params.crit == HIGH)
#define wcet_hi(t)		(tsk_rt(t)->task_params.exec_cost_hi)
#define wcet_lo(t)		(tsk_rt(t)->task_params.exec_cost)
#define get_actual_exec_time(t)	(tsk_rt(t)->task_params.actual_exec_time)


#endif
/*
 * scheduling lock slock
 * protects the domain and serializes scheduling decisions
 */
#define slock domain.ready_lock

} pfp_domain_t;

DEFINE_PER_CPU(pfp_domain_t, pfp_domains);

#define local_pfp		(&__get_cpu_var(pfp_domains))
#define remote_dom(cpu)		(&per_cpu(pfp_domains, cpu).domain)
#define remote_pfp(cpu)	(&per_cpu(pfp_domains, cpu))
#define task_dom(task)		remote_dom(get_partition(task))
#define task_pfp(task)		remote_pfp(get_partition(task))


/* we assume the lock is being held */
static void preempt(pfp_domain_t *pfp)
{
	preempt_if_preemptable(pfp->scheduled, pfp->cpu);
}

static unsigned int priority_index(struct task_struct* t)
{
	return get_priority(t);
}

static void pfp_release_jobs(rt_domain_t* rt, struct bheap* tasks)
{
	pfp_domain_t *pfp = container_of(rt, pfp_domain_t, domain);
	unsigned long flags;
	struct task_struct* t;
	struct bheap_node* hn;
	int tempp=0,tempp1=0;
	lt_t temppclock;

	raw_spin_lock_irqsave(&pfp->slock, flags);

	while (!bheap_empty(tasks)) {
		hn = bheap_take(fp_ready_order, tasks);
		t = bheap2task(hn);
#ifdef OCBP_PROTOCOL
		
		if(tempp==0)
		{
			temppclock = litmus_clock() + 20000000;
			tempp++;
		}	

		if(tempp1++ <3)
		{
			t->rt_param.job_params.release = temppclock;
		}

		if(pfp->scheduled == NULL && task_count == 3)
		{
			
			pfp->mode = lo_mode;
			TRACE("ocbp -- pfp schedule -- 1122 -- Going back to Lo Crit mode %d\n",pfp->mode);
			start_clock= litmus_clock();
			priority_ocbp(taskarray,JOB_COUNT_MAX);
			stop_clock= litmus_clock();
			TRACE("ocbp -- gmc -- 2623 -- Priority computed when job released when the system was in idle computation time is %lld\n",(stop_clock - start_clock));
			
			if(temp < 100)
			{
				temp1+=(stop_clock - start_clock);
				temp++;
			}
			if(temp == 100)
			{
				temp1=temp1/100;
				temp++;
			TRACE("ocbp -- gmc -- 2624 -- average computation time is %lld\n",temp1);

			}

		}

		if(t != NULL)
		TRACE("ocbp -- pfp schedule -- 1120 -- Task %d released from release queue in mode  %d at time %lld\n",t->rt_param.task_params.pid,pfp->mode,litmus_clock());
		if(t->rt_param.task_params.crit == 1)
		TRACE("Success - 3435 --Domain Initialized");
		if(t->rt_param.task_params.crit == 0)
		TRACE("Success - 3436 -- Domain Initialized");
		if(pfp->mode == hi_mode)
		TRACE("Success - 3437 -- Domain Initialized");




		if(pfp->mode == hi_mode && t->rt_param.task_params.crit == HIGH)
		{
			TRACE_TASK(t, "ocbp -- 1323 -- pid:%d; Crit mode:%d",
			  t->rt_param.task_params.pid , pfp->mode);
			fp_prio_add(&pfp->ready_queue, t, priority_index(t));
		}
		else if(pfp->mode == lo_mode)
		{		
			TRACE_TASK(t, "ocbp -- 1321 --released (part:%d prio:%d)\n",
				   get_partition(t), get_priority(t));
			fp_prio_add(&pfp->ready_queue, t, priority_index(t));
		}
		else if(pfp->mode == hi_mode && t->rt_param.task_params.crit == LOW)
		{
			TRACE_TASK(t, "ocbp -- 1322 --released old release time %lld\n",t->rt_param.job_params.release/1000000);
			t->rt_param.job_params.release =  litmus_clock() + busy_interval;
			TRACE_TASK(t, "ocbp -- 1322 --released new release time %lld\n",t->rt_param.job_params.release/1000000);

		}
		// TODO delay of low crit jobs when arrived in HIGM SYSTEM MODE	
#endif

	}

	/* do we need to preempt? */
	if (fp_higher_prio(fp_prio_peek(&pfp->ready_queue), pfp->scheduled)) {
		TRACE_CUR("preempted by new release\n");
		preempt(pfp);

	}

	raw_spin_unlock_irqrestore(&pfp->slock, flags);
}

static void pfp_preempt_check(pfp_domain_t *pfp)
{
	if (fp_higher_prio(fp_prio_peek(&pfp->ready_queue), pfp->scheduled))
		preempt(pfp);
}

static void pfp_domain_init(pfp_domain_t* pfp,
			       int cpu)
{
	fp_domain_init(&pfp->domain, NULL, pfp_release_jobs);
	pfp->cpu      		= cpu;
	pfp->scheduled		= NULL;
#ifdef OCBP_PROTOCOL
	pfp->mode = lo_mode;
	task_count = 0;			// gmc -- task_count is reset when the plugin activated
	TRACE_CUR("--ocbp gmc 8898 -- domain initialization\n");
	TRACE("Success -  Domain Initialized");

#endif
	fp_prio_queue_init(&pfp->ready_queue);
}

static void requeue(struct task_struct* t, pfp_domain_t *pfp)
{
	BUG_ON(!is_running(t));
	TRACE_TASK(t,"requeing task. Setting completed flag\n");
	tsk_rt(t)->completed = 0;
	if (is_released(t, litmus_clock()))
		fp_prio_add(&pfp->ready_queue, t, priority_index(t));
	else
		add_release(&pfp->domain, t); /* it has got to wait */
}

static void job_completion(struct task_struct* t, int forced)
{
	pfp_domain_t* 	pfp = local_pfp;
	sched_trace_task_completion(t,forced);
	TRACE_TASK(t, "job_completion()\n");

	tsk_rt(t)->completed = 0;
	prepare_for_next_period(t);
	if (is_released(t, litmus_clock()))
		sched_trace_task_release(t);
}

static struct task_struct* pfp_schedule(struct task_struct * prev)
{
	pfp_domain_t* 	pfp = local_pfp;
	struct task_struct*	next;

	int out_of_time, sleep, preempt, np, exists, blocks, resched, migrate;
#ifdef OCBP_PROTOCOL
	int hi_critical, service_time_ms, c_lo_ms, c_hi_ms, actual_exec_time_ms;
	lt_t service_time, c_lo, c_hi;
#endif
	raw_spin_lock(&pfp->slock);

	/* sanity checking
	 * differently from gedf, when a task exits (dead)
	 * pfp->schedule may be null and prev _is_ realtime
	 */
	BUG_ON(pfp->scheduled && pfp->scheduled != prev);
	BUG_ON(pfp->scheduled && !is_realtime(prev));

	/* (0) Determine state */
	exists      = pfp->scheduled != NULL;
	blocks      = exists && !is_running(pfp->scheduled);
#ifdef OCBP_PROTOCOL
//if(pfp->scheduled != NULL)
{
	/*if(!exists)
	{
	priority_ocbp(taskarray,JOB_COUNT_MAX);	
	TRACE("Priority computation as processor is idle");
	}*/
	TRACE("ocbp -- pfp_schedule -- Mode %d\n",pfp->mode);

	hi_critical = exists && task_critical_check_high(pfp->scheduled);
	TRACE("ocbp -- pfp_schedule --High Critical indicator %d\n",hi_critical);

	service_time = (!exists) ? 0 : get_exec_time(pfp->scheduled);
	service_time_ms = nstoms(service_time);
	TRACE("ocbp -- pfp_schedule -- Prev task Executed for %dms\n", service_time_ms);

	actual_exec_time_ms = (!exists) ? 0 : nstoms(get_actual_exec_time(pfp->scheduled));
	TRACE("ocbp -- pfp schedule -- actual exec time user supplied exec time  %dms\n",actual_exec_time_ms);

	c_lo = (!exists) ? 0 : wcet_lo(pfp->scheduled);
	c_lo_ms = nstoms(c_lo);
	TRACE("ocbp -- pfp_schedule -- c_lo for prev non exist task  %dms\n",c_lo_ms);

	c_hi = (!exists) ? 0 : wcet_hi(pfp->scheduled);
	c_hi_ms = nstoms(c_hi);
	TRACE("ocbp -- pfp_schedule -- c_hi for prev non exist task %dms\n",c_hi_ms);

}
	/*If any high critical task exhaust its budget then move to bailout mode.
	 *Calculate the fund as c_hi - c_low. Transition can be triggered either
	 *from normal mode or recovery mode. 
	 */
	if ((pfp->mode == lo_mode)  && (service_time_ms > c_lo_ms)) {
		pfp->mode = hi_mode;
		TRACE("ocbp -- pfp schedule -- 1121 -- Entering Hi Crit mode %d\n",pfp->mode);
	}
#endif

	out_of_time = exists &&
				  budget_enforced(pfp->scheduled) &&
				  budget_exhausted(pfp->scheduled);
	np 	    = exists && is_np(pfp->scheduled);
	sleep	    = exists && is_completed(pfp->scheduled);
	migrate     = exists && get_partition(pfp->scheduled) != pfp->cpu;
	preempt     = !blocks && (migrate || fp_preemption_needed(&pfp->ready_queue, prev));
	TRACE("Task parameters : out_of_time %d, np %d, sleep %d, blocking %d migrate %d, preempt %d\n",
		out_of_time, np, sleep, blocks, migrate, preempt);

	/* If we need to preempt do so.
	 * The following checks set resched to 1 in case of special
	 * circumstances.
	 */
	resched = preempt;

	/* If a task blocks we have no choice but to reschedule.
	 */
	if (blocks)
		resched = 1;

	/* Request a sys_exit_np() call if we would like to preempt but cannot.
	 * Multiple calls to request_exit_np() don't hurt.
	 */
	if (np && (out_of_time || preempt || sleep))
		request_exit_np(pfp->scheduled);

	/* Any task that is preemptable and either exhausts its execution
	 * budget or wants to sleep completes. We may have to reschedule after
	 * this.
	 * If overrun is allowed for critical task then dont resched even
	 * if the budget is exhausted
	 */
	if (!np && (out_of_time || sleep) && !blocks && !migrate) {
		TRACE_TASK(pfp->scheduled,"Job completes\n");
		job_completion(pfp->scheduled, !sleep);
		resched = 1;
	}

	/* The final scheduling decision. Do we need to switch for some reason?
	 * Switch if we are in RT mode and have no task or if we need to
	 * resched.
	 */
	next = NULL;
	if ((!np || blocks) && (resched || !exists)) {
		/* When preempting a task that does not block, then
		 * re-insert it into either the ready queue or the
		 * release queue (if it completed). requeue() picks
		 * the appropriate queue.
		 */
		if (pfp->scheduled && !blocks  && !migrate)
			requeue(pfp->scheduled, pfp);
		next = fp_prio_take(&pfp->ready_queue);
		TRACE_TASK(next,"picked next task from ready queue\n");
		if (next == prev) {
			struct task_struct *t = fp_prio_peek(&pfp->ready_queue);
			TRACE_TASK(next, "next==prev sleep=%d oot=%d np=%d preempt=%d migrate=%d "
				   "boost=%d empty=%d prio-idx=%u prio=%u\n",
				   sleep, out_of_time, np, preempt, migrate,
				   is_priority_boosted(next),
				   t == NULL,
				   priority_index(next),
				   get_priority(next));
			if (t)
				TRACE_TASK(t, "waiter boost=%d prio-idx=%u prio=%u\n",
					   is_priority_boosted(t),
					   priority_index(t),
					   get_priority(t));
		}
		/* If preempt is set, we should not see the same task again. */
		BUG_ON(preempt && next == prev);
		/* Similarly, if preempt is set, then next may not be NULL,
		 * unless it's a migration. */
		BUG_ON(preempt && !migrate && next == NULL);
	} else
		/* Only override Linux scheduler if we have a real-time task
		 * scheduled that needs to continue.
		 */
		if (exists)
			next = prev;

	if (next) {
			TRACE_TASK(next, "scheduled at %llu\n", litmus_clock());
	} else {
#ifdef OCBP_PROTOCOL
		if(pfp->mode == hi_mode)
		{
		//	pfp->mode = lo_mode;
		//	TRACE("ocbp -- pfp schedule -- 1122 -- Going back to Lo Crit mode %d\n",pfp->mode);

		}
#endif	
	}

/*if(pfp->mode == hi_mode && task_critical_check_high(next) )/{
	pfp->scheduled = next;
}
else if(pfp->mode == lo_mode)
{*/
	pfp->scheduled = next;
//}
	sched_state_task_picked();
	raw_spin_unlock(&pfp->slock);

	if(next != NULL && next != prev)
			{
				TRACE("ocbp -- pfp schedule -- 4444 -- scheduling %d prev task %d time %lld mode %d\n",next->rt_param.task_params.pid,prev->rt_param.task_params.pid,(litmus_clock()/1000000), pfp->mode);
				temp2=litmus_clock();
				TRACE("ocbp -- pfp schedule -- 5555 -- scheduling %d \n",prev->rt_param.task_params.pid);
			}
	return next;// TODO which is assigning to the run queue
}
 
/*	Prepare a task for running in RT mode
 */
static void pfp_task_new(struct task_struct * t, int on_rq, int is_scheduled)
{
	pfp_domain_t* 	pfp = task_pfp(t);
	unsigned long		flags;

	TRACE_TASK(t, "FPPS: task new, cpu = %d\n",
		   t->rt_param.task_params.cpu);
#ifndef OCBP_PROTOCOL
	release_at(t, litmus_clock());
#endif
#ifdef OCBP_PROTOCOL
	release_at(t, t->rt_param.task_params.release_time + litmus_clock());
	if( task_count < JOB_COUNT_MAX )
	{
		task_ocbp[task_count].deadline = t->rt_param.task_params.relative_deadline;
		task_ocbp[task_count].c_low = t->rt_param.task_params.exec_cost;
		task_ocbp[task_count].c_hi = t->rt_param.task_params.exec_cost_hi;
		task_ocbp[task_count].criticality = t->rt_param.task_params.crit;
		task_ocbp[task_count].prio = 0;//t->rt_param.task_params.priority;
		task_ocbp[task_count].period = t->rt_param.task_params.period;
		taskarray[task_count] = &task_ocbp[task_count];
		ocbp_task_address[task_count]=t;
		TRACE("ocbp -- gmc --Task no %d added \n",task_count);
		task_count = task_count + 1;
	
	if(task_count == JOB_COUNT_MAX) {
		busy_int(taskarray,JOB_COUNT_MAX);
		TRACE("ocbp -- gmc -- going to start done by function priority_ocbp \n");
		jobs_count_in_busy_interval(taskarray,JOB_COUNT_MAX);
		TRACE("ocbp --gmc -- no of jobs is %d\n",job_count_sum);
		priority_ocbp(taskarray,JOB_COUNT_MAX);
		TRACE("ocbp -- gmc -- priority computation done by function priority_ocbp \n");

	}
	}
	

#endif
	/* setup job parameters */
	TRACE("is_scheduled %d, on_rq %d\n",is_scheduled, on_rq);
	raw_spin_lock_irqsave(&pfp->slock, flags);
	if (is_scheduled) {
		/* there shouldn't be anything else running at the time */
		BUG_ON(pfp->scheduled);
		pfp->scheduled = t;
	} else if (on_rq) {
		requeue(t, pfp);
		/* maybe we have to reschedule */
		pfp_preempt_check(pfp);
	}
	raw_spin_unlock_irqrestore(&pfp->slock, flags);
}

static void pfp_task_wake_up(struct task_struct *task)
{
	unsigned long		flags;
	pfp_domain_t*		pfp = task_pfp(task);
	lt_t			now;

	TRACE_TASK(task, "wake_up at %llu\n", litmus_clock());
	raw_spin_lock_irqsave(&pfp->slock, flags);

	BUG_ON(is_queued(task));
	now = litmus_clock();
	if (is_sporadic(task) && is_tardy(task, now)) {
		/* new sporadic release */
		release_at(task, now);
		sched_trace_task_release(task);
	}

	/* Only add to ready queue if it is not the currently-scheduled
	 * task. This could be the case if a task was woken up concurrently
	 * on a remote CPU before the executing CPU got around to actually
	 * de-scheduling the task, i.e., wake_up() raced with schedule()
	 * and won. Also, don't requeue if it is still queued, which can
	 * happen under the DPCP due wake-ups racing with migrations.
	 */
	if (pfp->scheduled != task) {
		requeue(task, pfp);
		pfp_preempt_check(pfp);
	}

	raw_spin_unlock_irqrestore(&pfp->slock, flags);
	TRACE_TASK(task, "wake up done\n");
}

static void pfp_task_block(struct task_struct *t)
{
	/* only running tasks can block, thus t is in no queue */
	TRACE_TASK(t, "block at %llu, state=%d\n", litmus_clock(), t->state);

	BUG_ON(!is_realtime(t));

	/* If this task blocked normally, it shouldn't be queued. The exception is
	 * if this is a simulated block()/wakeup() pair from the pull-migration code path.
	 * This should only happen if the DPCP is being used.
	 */
	BUG_ON(is_queued(t));
}

static void pfp_task_exit(struct task_struct * t)
{
	unsigned long flags;
	pfp_domain_t* 	pfp = task_pfp(t);
	rt_domain_t*		dom;

	raw_spin_lock_irqsave(&pfp->slock, flags);
	if (is_queued(t)) {
		BUG(); /* This currently doesn't work. */
		/* dequeue */
		dom  = task_dom(t);
		remove(dom, t);
	}
	if (pfp->scheduled == t) {
		pfp->scheduled = NULL;
		preempt(pfp);
	}
	TRACE_TASK(t, "RIP, now reschedule\n");

	raw_spin_unlock_irqrestore(&pfp->slock, flags);
}

static long pfp_admit_task(struct task_struct* tsk)
{
	if (task_cpu(tsk) == tsk->rt_param.task_params.cpu &&
#ifdef CONFIG_RELEASE_MASTER
	    /* don't allow tasks on release master CPU */
	    task_cpu(tsk) != remote_dom(task_cpu(tsk))->release_master &&
#endif
	    litmus_is_valid_fixed_prio(get_priority(tsk)))
		return 0;
	else
		return -EINVAL;
}

static struct domain_proc_info pfp_domain_proc_info;
static long pfp_get_domain_proc_info(struct domain_proc_info **ret)
{
	*ret = &pfp_domain_proc_info;
	return 0;
}

static void pfp_setup_domain_proc(void)
{
	int i, cpu;
	int release_master =
#ifdef CONFIG_RELEASE_MASTER
		atomic_read(&release_master_cpu);
#else
		NO_CPU;
#endif
	int num_rt_cpus = num_online_cpus() - (release_master != NO_CPU);
	struct cd_mapping *cpu_map, *domain_map;

	memset(&pfp_domain_proc_info, sizeof(pfp_domain_proc_info), 0);
	init_domain_proc_info(&pfp_domain_proc_info, num_rt_cpus, num_rt_cpus);
	pfp_domain_proc_info.num_cpus = num_rt_cpus;
	pfp_domain_proc_info.num_domains = num_rt_cpus;
	for (cpu = 0, i = 0; cpu < num_online_cpus(); ++cpu) {
		if (cpu == release_master)
			continue;
		cpu_map = &pfp_domain_proc_info.cpu_to_domains[i];
		domain_map = &pfp_domain_proc_info.domain_to_cpus[i];

		cpu_map->id = cpu;
		domain_map->id = i; /* enumerate w/o counting the release master */
		cpumask_set_cpu(i, cpu_map->mask);
		cpumask_set_cpu(cpu, domain_map->mask);
		++i;
	}
}

static long pfp_activate_plugin(void)
{
#ifdef CONFIG_RELEASE_MASTER
	int cpu;

	for_each_online_cpu(cpu) {
		remote_dom(cpu)->release_master = atomic_read(&release_master_cpu);
	}
#endif
	TRACE("ocbp -- gmc 1312 --plugin activated \n");
	temp2=litmus_clock();		
	pfp_setup_domain_proc();

	return 0;
}

static long pfp_deactivate_plugin(void)
{
	destroy_domain_proc_info(&pfp_domain_proc_info);
	TRACE("ocbp -- gmc 1313 --plugin deactivated \n");

	return 0;
}

/*	Plugin object	*/
static struct sched_plugin pfp_plugin __cacheline_aligned_in_smp = {
	.plugin_name		= "OCBP",
	.task_new		= pfp_task_new,
	.complete_job		= complete_job,
	.task_exit		= pfp_task_exit,
	.schedule		= pfp_schedule,
	.task_wake_up		= pfp_task_wake_up,
	.task_block		= pfp_task_block,
	.admit_task		= pfp_admit_task,
	.activate_plugin	= pfp_activate_plugin,
	.deactivate_plugin	= pfp_deactivate_plugin,
	.get_domain_proc_info	= pfp_get_domain_proc_info,
};


static int __init init_ocbp(void)
{
	int i;

	/* We do not really want to support cpu hotplug, do we? ;)
	 * However, if we are so crazy to do so,
	 * we cannot use num_online_cpu()
	 */
	for (i = 0; i < num_online_cpus(); i++) {
		pfp_domain_init(remote_pfp(i), i);
	}
	return register_sched_plugin(&pfp_plugin);
}

module_init(init_ocbp);
